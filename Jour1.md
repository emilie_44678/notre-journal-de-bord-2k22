![depositphotos_103815556-stock-illustration-november-1-vector-flat-daily](/uploads/749363df878a20333c528761599f01e0/depositphotos_103815556-stock-illustration-november-1-vector-flat-daily.jpg)

**Le premier jour, la première familiarisation avec le jeu.**

 Des le début, nous avons commencer à interagir avec celui-ci nous avons compris les bases fondamentales. 
C'est alors que nous avons commencé par tout ce qui ai organisation et développement.

**Le jeu d'Awale**

Si au cas ou vous auriez oublié les règles nous allons vous l'es expliquer brièvement !

_Awalé_

L’awalé, appelé également Awélé, Wari ou awele fait partie (à l’instar du  Mancala) des jeux de stratégie les plus anciens. Son origine serait l’ancienne Égypte.
Ces jeux sont aujourd’hui  très populaires en Afrique et symbolisent l’importance de l’agriculture.

– But du jeu : S’emparer d’un maximum de graines. Le joueur qui a le plus de graines a la fin de la partie l’emporte.

– Nombre de joueurs :2 

– Préparation : Le plateau est placé horizontalement entre les deux joueurs. Chaque joueur place 4 graines dans chacun des 6 trous devant lui.


**Déroulement d’une partie d’awalé**

Plateau : La partie se joue sur le plateau de 2×6 trous.

Greniers : Si votre awalé contient deux greniers (trous plus grands à chacune des deux extrémités) alors ils ne comptent pas comme des trous, ils ne servent qu’à ranger les graines des joueurs.

 Le grenier d’un joueur se situant à sa droite.

Jouer un coup : Le 1er joueur prend toutes les graines de l’un des 6 trous se trouvant de son côté et en dépose une dans chaque trou suivant celui qu’il a vidé.

Sens de la partie : antihoraire.

Capture : si la dernière graine est déposée dans un trou de l’adversaire comportant déjà 1 ou 2 graines, le joueur capture les 2 ou 3 graines résultantes. Les graines capturées sont alors sorties du jeu (grenier) et le trou est laissé vide.

–> Rafle : Lorsqu’un joueur s’empare de 2 ou 3 graines, si la case précédente contient également 2 ou 3 graines, elles sont capturées aussi et ainsi de suite.

Boucle : Si le nombre de graines prises dans un trou de départ est supérieur à 11, cela fait que l’on va boucler un tour, auquel cas, à chaque passage, la case de départ est sautée et donc toujours laissée vide.

Affamer : Un joueur n’a pas le droit de jouer un coup qui prenne toutes les graines du camp adverse.

Fin du jeu : le jeu se termine lorsqu’un joueur n’a plus de graines dans son camp (et ne peut donc plus jouer). L’adversaire capture alors les graines restantes. 


Bien sûr nous, nous jouerons sur ordinateur contre L'IR.

![awale](/uploads/27052b8a240a3dcef4d482f1e84db756/awale.jpg)

**L'organisation**

 Cylian et moi-même avons dispache les taches, Cylian commença par créer des petites fonctions permettant de modéliser le jeu plus facilement et d'avoir dès le début une base solide. `

Et moi de mon côté je m'occupais de toutes les règles que l'on devait impérativement mettre dans le jeu, et toute ses variantes que le jeu contenait.
**Résultat du premier jour **

```class Trou:
    def __init__(self, i):
        self.les_graines = ["0" for i in range(4)] 
        self.nb_graines = len(self.les_graines)
        self.index = i

class Tablier:
    def __init__(self):
        self.les_trous = [Trou(i) for i in range(1,13)]

def Initialisation():
    T = Tablier()
    return T

def enlever_graines(trou_où_enlever):
    gs = trou_où_enlever.les_graines
    trou_où_enlever.les_graines = []
    trou_où_enlever.nb_graines = 0
    return(gs)

def placer_graine(g, trou_où_ajouter):
    trou_où_ajouter.les_graines.append(g)
    trou_où_ajouter.nb_graines += 1

def déplacer_graines(sens_de_rotation, trou, T):
    gs = enlever_graines(trou)
    index = trou.index
    for elt in gs:
        index += sens_de_rotation
        placer_graine(elt, T.les_trous[index])
    return(index)

def reprendre_graines(T, index, camp):
    

def jeu_d_Awale(sens_de_rotation, joueur_début):
    T = Initialisation()
    sens_de_rotation = input("quel sens de rotation voulez-vous ? 1 pour le sens des aiguilles d'une montre et -1 pour le sens contraire ")
    joueur_début = input("Qui débute ? vous ou IA")
    camp_joueur, camp_ia = [1, 6], [7,12] 
    while ```

Nous avons créer une classe <<class Trou>>, qui avait comme methodes <<def__init__>> puis nous avons créer une autre classe <<class Tablier>> qui va servir à la modélisation dui plateau mais aussi à tout les mouvements que l'on peut effectuer cet à dire : deplacer les graines que cela soit dans le sens des aiguilles d'une montre ou dans le sens inverse. Mais, aussi il faut avoir une méthode qui nous permet d'enlever les graines.

 Celle ci à été diffcile à definir c'est pour cela que nous avons eu l'idée de commencer notre grosse méthode la méthode <<le jeu d'awalé >> (la méthode enlever_graines est disponible dès le jour 2)

cette méthode, nous permet entre autres de modéliser de 1 : le sens de rotation que le joueur sélectionnera et non l'IA (nous avons décider de le faire ainsi pour laisser au moins une chance au joueur de gagner contre l'ordinateur) mais pour ne pas nous inculper de tricherie et d'aide envers le joueur nous avons décider de demander au joueur si il voulait commencer ou l'IA; (espérons qu'il dise lui, un peu de stratégie ;) )


